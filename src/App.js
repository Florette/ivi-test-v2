import React from 'react';
import './styles/main.scss';
import Crystal from "./components/Crystal";

function App() {
  return (
    <div className="wrapper">
        <Crystal />
    </div>
  );
}

export default App;
