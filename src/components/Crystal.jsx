import React, {Component} from 'react';
import './Crystal.scss';

export default class Crystal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            count: 0
        }
    };

    componentDidMount() {
        let _this = this;
        let delay = 3000;

        this.interval = setTimeout(function go() {
            _this.setState({
                    count: _this.state.count === 3 ? 0 : _this.state.count + 1
                }
            );
            delay = _this.state.count !== 0 ? 1000 : 3000;
            setTimeout(go, delay)

        }, delay)
    };

    componentWillUnmount() {
        clearTimeout(this.interval);
    };

    toggleClass = () => this.setState({active: !this.state.active});

    render() {
        const {active, count} = this.state;
        return (
            <div className="crystal">
                <div className="crystal__knife">
                    <div className={`crystal__figure ${active ? 'active' : ''}`} onClick={this.toggleClass}>
                        <div className="crystal__figure_block">
                            <ul className="crystal__figure_block-content">
                                <li className="content show">{count || 'Go!'}</li>
                            </ul>
                        </div>
                    </div>
                    <div className="crystal__background"/>
                </div>
            </div>
        )
    }
}